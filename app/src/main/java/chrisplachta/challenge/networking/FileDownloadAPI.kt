package chrisplachta.challenge.networking

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET


interface FileDownloadAPI {

    @GET("/dealerinspire/ios-code-challenge/raw/master/Apache.log")
    //@Streaming
    fun downloadApacheLogStream(): Observable<ResponseBody>
}