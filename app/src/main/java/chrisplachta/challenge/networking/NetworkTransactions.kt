package chrisplachta.challenge.networking

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okio.BufferedSource
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException


open class NetworkTransactions {

    open val api: FileDownloadAPI by lazy {
        Retrofit.Builder()
            .baseUrl("https://bitbucket.org")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create<FileDownloadAPI>(FileDownloadAPI::class.java)
    }

    fun downloadApacheFile(observer: Observer<String>, subscribeScheduler: Scheduler = Schedulers.io(), observeScheduler: Scheduler = AndroidSchedulers.mainThread()) {
        api.downloadApacheLogStream()
            .flatMap { responseBody -> getLinesOfInputFromSource(responseBody.source()) }
            .subscribeOn(subscribeScheduler)
            .observeOn(observeScheduler)
            .subscribe(observer)
    }

    private fun getLinesOfInputFromSource(source: BufferedSource): Observable<String> {
        return Observable.create { emitter ->
            try {
                while (!source.exhausted()) {
                    source.readUtf8Line()?.let { fileChunk ->
                        emitter.onNext(fileChunk)
                    }
                }
                emitter.onComplete()
            } catch (ex: IOException) {
                ex.printStackTrace()
                emitter.onError(ex)
            }
        }
    }
}